# README #

Aplicação Java para o processo seletivo da CI&T


###Tecnologias utilizadas ###

- O framework utilizado foi o Spring Framework, devido sua consistencia e ampla utilização, além dos constantes suportes que recebe.
- Para build e controle de depencias foi utilizado maven devido sua facilidade e robustez
- A aplicação foi feita com Spring Boot
- Base de dados h2. Uma base de dados em memória fácil de usar e integrada ao Spring
- Para os testes unitários foi utilizado o JUnit


### Organização do código ###

- As propriedades do projeto, em relação as configurações do spring boot se encontram no arquivo properties.yml no diretorio src/main/resources
- Na pasta src/main/java estão contidos os códigos fonte do projeto sendo o pacote base com.ciandt.app
 	- o pacote domain contém as entidades do projeto
	- o pacote resource contám os controllers de endpoit da API
	- o pacote service contém as regras de negócio para inserção e pesquisa
 	- o pacote exceptions contém as exceptions que a api pode disparar
	- o pacote exceptions.handler contém a implementação de tratamento das exceptions da api
	- o pacote repository contém as interfaces de JpaRepository para acesso à base de dados
	- o pacote util contém algumas classes auxiliares
- No diretório src/main/resources/templates contém exemplos de jsons que a api consome
- No diretório scr/test se encontram os testes unitários feitos em JUnit


### Como executar ###

- pode-se executar com qualquer ide com suporte ao spring
- pode-se executar pelo mvn com o comando mvn spring-boot:run


### API Endpoint e dicas de uso ###

api endereço base: http://localhost:8080

GET /competicao/setupTeste
	Retorno - Cadastra alguns locais e países para teste e devolve a lista dos mesmos para consultar os ids

POST /competicao 
	Body - uma competicao (json de exmplo na pasta src/resource/templates/competicao.json)
	Retorna - persiste a competicao na base e a retorna Status - 201 
	Possíveis retornos de erro: CampoNuloException (Status 400), DuracaoInvalidaException (Status 422), HoraIndisponivelException (Status 409), MaximoCompeticoesException (Status 409), PaisInexistenteException (Status 400). LocalInexistenteException (Status 400), OponentesInvalidosException (Status 400)

POST /local 
	Body - um local (json de emplo na pasta src/resource/templates/local.json 
	Retorna - pesiste o local e o retorna Status - 201

POST /pais 
	Body - um pais (json de emplo na pasta src/resource/templates/pais.json 
	Retorna - persiste o pais e o retorna Status - 201

GET /competicao?modalidade= 
	Retorna - lista de competições filtradas ou não pela modalidade, dependendo se a mesma foi passada na variavel modalidade na url
	Status - 200 
	Possíveis retornos de erro: ModalidadeInvalidaException (Status 400) e CompeticaoNaoEncontradaException (Status 404)

O endpoint GET /competicao/setupTeste serve para pré inserir os dados de Local e País na base, para que se possa inserir as competições