package com.ciandt.app.services;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.ciandt.app.domain.Competicao;
import com.ciandt.app.domain.Etapa;
import com.ciandt.app.domain.Local;
import com.ciandt.app.domain.Modalidade;
import com.ciandt.app.domain.Pais;
import com.ciandt.app.exceptions.CompeticaoNaoEncontradaException;
import com.ciandt.app.exceptions.DuracaoInvalidaException;
import com.ciandt.app.exceptions.HorarioIndisponivelException;
import com.ciandt.app.exceptions.MaximoDeCompeticoesException;
import com.ciandt.app.exceptions.ModalidadeInvalidaException;
import com.ciandt.app.exceptions.OponentesInvalidosException;
import com.ciandt.app.service.CompeticaoService;
import com.ciandt.app.util.CompeticaoValidacao;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CompeticaoServiceTest {

	@Autowired
	private CompeticaoService competicaoService;
	
	@Autowired
	private EntityManager manager;
	
	private final Local local1 = new Local("Tokyo Domme","endereco Tokyo Domme");
	
	private final Local local2 = new Local("Ota Stadium","endereco Ota Stadium");
	
	private final Pais pais1 = new Pais("Japao");
	
	private final Pais pais2 = new Pais("Brasil");
	
	/*
	 * Variáveis auxiliares para definir as datas e horas 
	 */
	private Time inicio1;
	private Time inicio2;
	private Time inicio3;
	private Time inicio4;
	private Time fim1;
	private Time fim2;
	private Time fim3;
	private Time fim4;
	private Date dia1;
	private Date dia2;
	private Date dia3;
	
	@Before
	public void setUp() {
		manager.persist(local1);
		manager.persist(local2);
		manager.persist(pais1);
		manager.persist(pais2);
		System.out.println("objetos estáticos instanciados na base");
		
		inicio1 = Time.valueOf("11:00:00");
		inicio2 = Time.valueOf("12:00:00");
		inicio3 = Time.valueOf("13:00:00");
		inicio4 = Time.valueOf("14:00:00");
		
		fim1 = Time.valueOf("12:00:00");
		fim2 = Time.valueOf("13:00:00");
		fim3 = Time.valueOf("14:00:00");
		fim4 = Time.valueOf("14:20:00");
		
		dia1 = Date.valueOf("2017-08-11");
		dia2 = Date.valueOf("2017-08-12");
		dia3 = Date.valueOf("2017-08-13");
		
		System.out.println("Horários e dias preenchidos");
	}
	

	/*
	 * Teste unitário para horário indisponível.
	 * O objeto comp1 será cadastrado com o local1, dia1 e inicio1 e fim3. 
	 * O objeto comp2 será cadastrado com o local1, dia1 e inicio2 e fim3
	 * A exception HorarioIndisponivelException deve ser disparada pois o objeto comp2 inicia antes do fim de comp1
	 * para o mesmo dia e local
	 */
	@Test(expected=HorarioIndisponivelException.class)
	@Transactional
	public void horarioIndisponivelException() {
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio1);
		comp1.setFim(fim3);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
		
		Competicao comp2 = new Competicao();
		comp2.setDia(dia1);
		comp2.setInicio(inicio2);
		comp2.setFim(fim3);
		comp2.setPaisUm(pais1.getNome());
		comp2.setPaisDois(pais2.getNome());
		comp2.setLocal(local1);
		comp2.setModalidade(Modalidade.FUTEBOL);
		comp2.setEtapa(Etapa.ELIMINATORIAS);
		comp2 = competicaoService.salvarCompeticao(comp2);
	}
	
	
	
	/*
	 * Teste unitário da duração mínima da competição.
	 * Inicio4 é 14:00:00 e fim4 é 14:20:00. Diferença de 20 minutos, portanto a 
	 * Exception DuracaoInvalida deve disparar
	 */
	@Test(expected=DuracaoInvalidaException.class)
	@Transactional
	public void duracaoInvalidaException() {
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio4);
		comp1.setFim(fim4);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
	}
	
	/*
	 * Teste unitário para o cálculo da duração.
	 * Pega o valor de inicio4 (14:00:00) e soma ao valor da duração mínima, definido na classe CompeticaoValidacao
	 * O calculo de intervalo de inicio4 e inicio4+duracao minima deve ser igual à duração mínima
	 */
	@Test
	@Transactional
	public void testeDoCalculoDeDuracao() {
		//recupera o valor da duraçã mínima
		long duracao = CompeticaoValidacao.duracaoMinima;
		//obtém o inicio4 e soma a duração mínima ao mesmo
		LocalTime tempo2 = inicio4.toLocalTime();
		tempo2 = tempo2.plusMinutes(duracao);
		//converte o novo tempo para Time
		Time temp = Time.valueOf(tempo2);
		System.out.println("AKDOSAOSJDIOAJISDIAJSDIAJSIDAIOSDOIASJDIOASDSAD");
		System.out.println(temp.toString());
		System.out.println("AKDOSAOSJDIOAJISDIAJSDIAJSIDAIOSDOIASJDIOASDSAD");
		//cacula a duração
		long tempoEntreInicioFim = CompeticaoValidacao.calcularDuracao(inicio4, temp);
		
		assertEquals(duracao,tempoEntreInicioFim);
		
	}
	
	
	/*
	 * Teste unitário para a quantidade máxima de competições por dia, modalidade e local.
	 * Todas as competições criadas são no local1, modalidade futebol e dia1
	 * 
	 *  Na quinta inserção a exception MaximoDeCompeticoesException deve ser disparada
	 */
	@Test(expected=MaximoDeCompeticoesException.class)
	@Transactional
	public void limiteDeCompeticoesNoDiaParaOLocalException() {
		//Comp1: 11:00:00 - 12:00:00
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio1);
		comp1.setFim(fim1);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
		
		//Comp2: 12:00:00 - 13:00:00
		Competicao comp2 = new Competicao();
		comp2.setDia(dia1);
		comp2.setInicio(inicio2);
		comp2.setFim(fim2);
		comp2.setPaisUm(pais1.getNome());
		comp2.setPaisDois(pais2.getNome());
		comp2.setLocal(local1);
		comp2.setModalidade(Modalidade.FUTEBOL);
		comp2.setEtapa(Etapa.ELIMINATORIAS);
		comp2 = competicaoService.salvarCompeticao(comp2);
		
		//Comp3: 13:00:00 - 14:00:00
		Competicao comp3 = new Competicao();
		comp3.setDia(dia1);
		comp3.setInicio(inicio3);
		comp3.setFim(fim3);
		comp3.setPaisUm(pais1.getNome());
		comp3.setPaisDois(pais2.getNome());
		comp3.setLocal(local1);
		comp3.setModalidade(Modalidade.FUTEBOL);
		comp3.setEtapa(Etapa.ELIMINATORIAS);
		comp3 = competicaoService.salvarCompeticao(comp3);
		
		//Comp4: 14:00:00 - 15:00:00
		Competicao comp4 = new Competicao();
		comp4.setDia(dia1);
		comp4.setInicio(inicio4);
		comp4.setFim(Time.valueOf(inicio4.toLocalTime().plusHours(1)));
		comp4.setPaisUm(pais1.getNome());
		comp4.setPaisDois(pais2.getNome());
		comp4.setLocal(local1);
		comp4.setModalidade(Modalidade.FUTEBOL);
		comp4.setEtapa(Etapa.ELIMINATORIAS);
		comp4 = competicaoService.salvarCompeticao(comp4);
		
		//Comp4: 15:00:00 - 16:00:00
		Competicao comp5 = new Competicao();
		comp5.setDia(dia1);
		comp5.setInicio(Time.valueOf(inicio4.toLocalTime().plusHours(2)));
		comp5.setFim(Time.valueOf(inicio4.toLocalTime().plusHours(3)));
		comp5.setPaisUm(pais1.getNome());
		comp5.setPaisDois(pais2.getNome());
		comp5.setLocal(local1);
		comp5.setModalidade(Modalidade.FUTEBOL);
		comp5.setEtapa(Etapa.ELIMINATORIAS);
		comp5 = competicaoService.salvarCompeticao(comp5);
	}

	
	/*
	 * Teste unitário para certificar-se de que dois países só podem fazer parte da mesma competição nas finais ou semifinais
	 */
	@Test(expected = OponentesInvalidosException.class)
	@Transactional
	public void paisesIguaisSemSerSemifinaisOuFinais() {
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio1);
		comp1.setFim(fim1);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
		
		assertNotNull(comp1);
		
		Competicao comp2 = new Competicao();
		comp2.setDia(dia2);
		comp2.setInicio(inicio1);
		comp2.setFim(fim1);
		comp2.setPaisUm(pais1.getNome());
		comp2.setPaisDois(pais1.getNome());
		comp2.setLocal(local1);
		comp2.setModalidade(Modalidade.FUTEBOL);
		comp2.setEtapa(Etapa.ELIMINATORIAS);
		comp2 = competicaoService.salvarCompeticao(comp2);
	}
	
	/*
	 * Teste unitário para o método de busca sem modalidade informada.
	 * Todas as competições salvas anteriormente devem estar presentes no retorno
	 */
	@Test
	@Transactional
	public void buscarCompeticoesSemModalidade() {
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio1);
		comp1.setFim(fim1);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
		
		Competicao comp2 = new Competicao();
		comp2.setDia(dia2);
		comp2.setInicio(inicio1);
		comp2.setFim(fim1);
		comp2.setPaisUm(pais1.getNome());
		comp2.setPaisDois(pais2.getNome());
		comp2.setLocal(local1);
		comp2.setModalidade(Modalidade.FUTEBOL);
		comp2.setEtapa(Etapa.ELIMINATORIAS);
		comp2 = competicaoService.salvarCompeticao(comp2);
		
		List<Competicao> comps = competicaoService.buscarCompeticoes("");
		
		assertTrue(comps.containsAll(Arrays.asList(comp1,comp2)));
	}
	
	
	/*
	 * Teste unitário do método de busca com modalidade fornecida
	 * Apenas a insersão correspondente à modalidade devem estar contidas no retorno	 *  
	 */
	@Test
	@Transactional
	public void buscarCompeticoesComModalidade() {
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio1);
		comp1.setFim(fim1);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
		
		Competicao comp2 = new Competicao();
		comp2.setDia(dia2);
		comp2.setInicio(inicio1);
		comp2.setFim(fim1);
		comp2.setPaisUm(pais1.getNome());
		comp2.setPaisDois(pais2.getNome());
		comp2.setLocal(local1);
		comp2.setModalidade(Modalidade.HIPISMO);
		comp2.setEtapa(Etapa.ELIMINATORIAS);
		comp2 = competicaoService.salvarCompeticao(comp2);
		
		List<Competicao> comps = competicaoService.buscarCompeticoes("futebol");
		
		assertFalse(comps.containsAll(Arrays.asList(comp1,comp2)));
	}
	
	/*
	 * Ao passar uma string que não representa uma modalidade a exception ModalidadeInvalidaException deve ser disparada
	 */
	@Test(expected=ModalidadeInvalidaException.class)
	@Transactional
	public void buscarCompeticoesComModalidadeInvalida() {
		
		List<Competicao> comps = competicaoService.buscarCompeticoes("modalidade desconehcida para o teste");
	}
	
	
	/*
	 * Teste unitário para busca de competições que não retornam nenhum valor
	 * A exception CompeticaoNaoEncontradaException deve ser disparada 
	 */
	@Test(expected = CompeticaoNaoEncontradaException.class)
	@Transactional
	public void buscarCompeticoesComModalidadeSemEncontrarResultados() {
		Competicao comp1 = new Competicao();
		comp1.setDia(dia1);
		comp1.setInicio(inicio1);
		comp1.setFim(fim1);
		comp1.setPaisUm(pais1.getNome());
		comp1.setPaisDois(pais2.getNome());
		comp1.setLocal(local1);
		comp1.setModalidade(Modalidade.FUTEBOL);
		comp1.setEtapa(Etapa.ELIMINATORIAS);
		comp1 = competicaoService.salvarCompeticao(comp1);
		
		Competicao comp2 = new Competicao();
		comp2.setDia(dia2);
		comp2.setInicio(inicio1);
		comp2.setFim(fim1);
		comp2.setPaisUm(pais1.getNome());
		comp2.setPaisDois(pais2.getNome());
		comp2.setLocal(local1);
		comp2.setModalidade(Modalidade.HIPISMO);
		comp2.setEtapa(Etapa.ELIMINATORIAS);
		comp2 = competicaoService.salvarCompeticao(comp2);
		
		List<Competicao> comps = competicaoService.buscarCompeticoes("volei");
		
		
	}
	
}
