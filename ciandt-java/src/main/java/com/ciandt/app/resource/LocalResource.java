package com.ciandt.app.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ciandt.app.domain.Local;
import com.ciandt.app.repository.LocalRepository;

@RestController
@RequestMapping(value="/local")
public class LocalResource {

	@Autowired
	private LocalRepository localRepo;
	
	@RequestMapping(method = RequestMethod.POST)	
	public ResponseEntity<?> cadastrarLocal(@RequestBody Local local){
		return ResponseEntity.status(HttpStatus.CREATED).body(localRepo.save(local));
	}
}
