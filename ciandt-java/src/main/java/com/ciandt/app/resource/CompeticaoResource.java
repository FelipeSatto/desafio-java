package com.ciandt.app.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ciandt.app.domain.Competicao;
import com.ciandt.app.domain.Local;
import com.ciandt.app.domain.Modalidade;
import com.ciandt.app.domain.Pais;
import com.ciandt.app.service.CompeticaoService;
import com.ciandt.app.util.PreCadastro;


/*
 * Rest controler para criação e consulta de competições
 * */
@RestController
@RequestMapping(value="competicao")
public class CompeticaoResource {

	@Autowired
	private CompeticaoService competicaoService;

	/*--------------------------------------------------------------------------*
	 *                                                                          *
	 * Recursos para criação de competições                                     *
	 * 																			*
	 *--------------------------------------------------------------------------*/ 
	 
	
	/*
	 * Recurso para criar uma nova competição
	 * 
	 * Paramêtros:
	 * 	- novaComp: objeto contendo as informações da competição a ser criada
	 * 
	 * Retorno:
	 * 	- Objeto persistido na base e status 201
	 * */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> criarCompeticao(@RequestBody Competicao novaComp ){
		return ResponseEntity.status(HttpStatus.CREATED).body(competicaoService.salvarCompeticao(novaComp));
	}

	/*--------------------------------------------------------------------------*
	 *                                                                          *
	 * Recursos para pesquisa de competições                                    *
	 *                                                                          *
	 *--------------------------------------------------------------------------*/
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> pesquisarCompeticoes(@RequestParam("modalidade")String mod){
		return ResponseEntity.status(HttpStatus.OK).body(competicaoService.buscarCompeticoes(mod));
	}
	
	/*--------------------------------------------------------------------------*
	 * 																			*
	 * Recursos auxiliares														*
	 * 																			*
	 *--------------------------------------------------------------------------*/
	
	
	/*
	 * Recurso auxiliar e opcional para pré-cadastrar os locais e países válidos da competição
	 * */
	@RequestMapping(value="/setupTeste",method = RequestMethod.GET)
	public ResponseEntity<?> criarLocais(){
		PreCadastro dadosCadastrados = new PreCadastro();
		List<Local> locais = competicaoService.criarLocaisParaTeste();
		List<Pais> paises = competicaoService.criarPaisesParaTeste();
		dadosCadastrados.setLocais(locais);
		dadosCadastrados.setPaises(paises);
		return ResponseEntity.status(HttpStatus.CREATED).body(dadosCadastrados);
	}
}
