package com.ciandt.app.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ciandt.app.domain.Local;
import com.ciandt.app.domain.Pais;
import com.ciandt.app.repository.LocalRepository;
import com.ciandt.app.repository.PaisRepository;

@RestController
@RequestMapping(value="/pais")
public class PaisResource {
	
	@Autowired
	private PaisRepository paisRepo;
	
	@RequestMapping(method = RequestMethod.POST)	
	public ResponseEntity<?> cadastrarPais(@RequestBody Pais pais){
		return ResponseEntity.status(HttpStatus.CREATED).body(paisRepo.save(pais));
	}
}
