package com.ciandt.app.repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ciandt.app.domain.Competicao;
import com.ciandt.app.domain.Local;
import com.ciandt.app.domain.Modalidade;

public interface CompeticaoRepository extends JpaRepository<Competicao, Long>{
	//Retorna as competições para o dia e local informados
	List<Competicao> findByDiaAndLocal(Date dia,Local local);
	//Retorna a competição que seja no dia e local informados e esteja no intervalo inicio e fim informados
	
	//List<Competicao> findByDiaAndLocalAndModalidadeAndInicioGreaterThanEqualAndInicioLessThanOrFimGreaterThanAndFimLessThanEqualOrInicioLessThanAndFimLessThanEqual(LocalDate dia,Local local,Modalidade modaidade,LocalTime inicio,LocalTime fim, LocalTime inicio2,LocalTime fim2,LocalTime inicio3,LocalTime fim3);
	//List<Competicao> findByDiaAndLocalAndModalidadeAndInicioGreaterThanEqualAndInicioLessThanAndFimGreterThanAndFimLessThanEqualAndInicioLessThanAndFimGreaterThan(LocalDate dia,Local local,Modalidade modalidade,Localtime inicio,LocalTime fim,LocalTime inicio2,LocalTime fim2,Locatime inicio3,LocalTime inicio4);
	//List<Competicao> findByDiaAndLocalAndModalidadeAndInicioGreaterThanEqualAndInicioLessThan_FimGreaterThanAndFimLessThanEqual_InicioLessThanAndFimGreaterThan(LocalDate dia,Local local,Modalidade modalidade,LocalTime inicio,LocalTime fim,LocalTime inicio2,LocalTime fim2,LocalTime inicio3,LocalTime inicio4);
	//List<Competicao> findByDia_InicioGreaterThanEqualAndInicioLessThan(LocalTime inicio,LocalTime fim);
	@Query(value = "Select * from competicao c where c.dia = ?1 and c.local_id = ?2 and c.modalidade = ?3 and "
			+ "((c.inicio >= ?4 and c.inicio<?5) or"
			+ "(c.fim > ?4 and c.fim <= ?5) or"
			+ "(c.inicio < ?4 and c.fim > ?4))",nativeQuery=true)
	List<Competicao> findPorHorarioDiaLocalModalidade(Date dia,Local local,String modalidade,String inicio,String fim);
	List<Competicao> findByModalidadeOrderByDiaAscInicioAsc(Modalidade modalidade);
	
	
	@Query(value = "SELECT * FROM competicao ORDER BY dia,inicio",nativeQuery = true)
	List<Competicao> findAllOrderByDiaAndInicio();

}
