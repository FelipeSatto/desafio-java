package com.ciandt.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ciandt.app.domain.Pais;

public interface PaisRepository extends JpaRepository<Pais, String>{
	
	Pais findByNomeIgnoreCase(String nome);

}
