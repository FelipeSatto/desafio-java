package com.ciandt.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ciandt.app.domain.Local;

public interface LocalRepository extends JpaRepository<Local,Long> {
	

}
