package com.ciandt.app.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Modalidade {
	FUTEBOL("futebol"),BASQUETE("basquete"),VOLEI("volei"),HIPISMO("hipismo"),JUDO("judo"),TIROAOALVO("tiro ao alvo");
	
	private String type;
	private int value;
	
	private Modalidade(final String type) {
		this.type = type;
	}
	
	private Modalidade(final int value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return this.type;
	}
}
