package com.ciandt.app.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Competicao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4968391129098288477L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "ASIA/Tokyo")
	@Column(nullable = false)
	private Date dia;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "ASIA/Tokyo")
	@Column(nullable = false)
	private Time inicio;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "ASIA/Tokyo")
	@Column(nullable = false)
	private Time fim;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "local_id", nullable=true)
	@JsonBackReference
	private Local local;
	
	@Column(nullable = false)
	private String paisUm;
	
	@Column(nullable = false)
	private String paisDois;
	
	@Enumerated(EnumType.STRING)
	private Modalidade modalidade;
	
	@Enumerated(EnumType.STRING)
	private Etapa etapa;
	
	public Competicao() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDia() {
		return dia;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public Time getInicio() {
		return inicio;
	}

	public void setInicio(Time inicio) {
		this.inicio = inicio;
	}

	public Time getFim() {
		return fim;
	}

	public void setFim(Time fim) {
		this.fim = fim;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public String getPaisUm() {
		return paisUm;
	}

	public void setPaisUm(String paisUm) {
		this.paisUm = paisUm;
	}

	public String getPaisDois() {
		return paisDois;
	}

	public void setPaisDois(String paisDois) {
		this.paisDois = paisDois;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

}
