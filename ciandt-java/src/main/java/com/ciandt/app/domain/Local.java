package com.ciandt.app.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Local implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1211505207790426673L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;
	private String endereco;
	
	@JsonIgnore
	@OneToMany(mappedBy = "local", cascade=CascadeType.REMOVE,  fetch = FetchType.LAZY)
	@JsonManagedReference
	private Set<Competicao> competicao;
	
	public Local() {
		
	}

	public Local(String nome, String endereco) {
		super();
		this.nome = nome;
		this.endereco = endereco;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Set<Competicao> getCompeticao() {
		return competicao;
	}

	public void setCompeticao(Set<Competicao> competicao) {
		this.competicao = competicao;
	}
	
}
