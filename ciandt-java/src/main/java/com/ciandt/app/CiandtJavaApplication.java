package com.ciandt.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CiandtJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiandtJavaApplication.class, args);
	}
}
