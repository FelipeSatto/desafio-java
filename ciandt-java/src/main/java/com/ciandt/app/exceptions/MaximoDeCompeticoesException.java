package com.ciandt.app.exceptions;

public class MaximoDeCompeticoesException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388500524939842666L;
	
	public MaximoDeCompeticoesException() {
		super();
	}
	
	public MaximoDeCompeticoesException(String mensagem) {
		super(mensagem);
	}

}
