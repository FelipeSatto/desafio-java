package com.ciandt.app.exceptions;

public class ModalidadeInvalidaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8766993743391128123L;
	
	public ModalidadeInvalidaException() {
		super();
	}
	
	public ModalidadeInvalidaException(String mensagem) {
		super(mensagem);
	}

}
