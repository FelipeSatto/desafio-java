package com.ciandt.app.exceptions;

public class CompeticaoNaoEncontradaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9045752721115019746L;
	
	public CompeticaoNaoEncontradaException() {
		super();
	}
	
	public CompeticaoNaoEncontradaException(String mensagem) {
		super(mensagem);
	}

}
