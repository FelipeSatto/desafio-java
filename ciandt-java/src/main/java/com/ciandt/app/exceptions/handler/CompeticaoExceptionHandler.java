package com.ciandt.app.exceptions.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ciandt.app.exceptions.CampoNuloException;
import com.ciandt.app.exceptions.CompeticaoNaoEncontradaException;
import com.ciandt.app.exceptions.DuracaoInvalidaException;
import com.ciandt.app.exceptions.HorarioIndisponivelException;
import com.ciandt.app.exceptions.LocalInexistenteException;
import com.ciandt.app.exceptions.MaximoDeCompeticoesException;
import com.ciandt.app.exceptions.ModalidadeInvalidaException;
import com.ciandt.app.exceptions.OponentesInvalidosException;
import com.ciandt.app.exceptions.PaisInexistenteException;
import com.ciandt.app.util.MensagemDeErro;


/*
 * Classe responsável por tratar as exceptions e retornar a mensagem, título e status adequado para o front-end 
 */
@ControllerAdvice
public class CompeticaoExceptionHandler {
	
	@ExceptionHandler(CampoNuloException.class)
	public ResponseEntity<MensagemDeErro> campoNuloExceptionHandler(CampoNuloException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Campo nulo",e.getMessage(),400,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
	}
	
	@ExceptionHandler(DuracaoInvalidaException.class)
	public ResponseEntity<MensagemDeErro> duracaoInvalidaExceptionHandler(DuracaoInvalidaException e,org.apache.catalina.servlet4preview.http.HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Duração inválida",e.getMessage(),422,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(msg);		
	}
	
	@ExceptionHandler(HorarioIndisponivelException.class)
	public ResponseEntity<MensagemDeErro> horarioIndisponivelExceptionHandler(HorarioIndisponivelException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Horário Indisponível",e.getMessage(),409,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body(msg);
	}
	
	@ExceptionHandler(MaximoDeCompeticoesException.class)
	public ResponseEntity<MensagemDeErro> maximoDeCompeticoesExceptionHandler(MaximoDeCompeticoesException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Máximo de competições atingido",e.getMessage(),409,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body(msg);
	}
	
	@ExceptionHandler(ModalidadeInvalidaException.class)
	public ResponseEntity<MensagemDeErro> modalidadeInvalidaExceptionHandler(ModalidadeInvalidaException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Modalidade inválida",e.getMessage(),400,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
	}
	
	@ExceptionHandler(CompeticaoNaoEncontradaException.class)
	public ResponseEntity<MensagemDeErro> competicaoNaoEncontradaExceptionHandler(CompeticaoNaoEncontradaException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Competição(ões) não encontrada(s)",e.getMessage(),404,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(msg);
	}
	
	@ExceptionHandler(PaisInexistenteException.class)
	public ResponseEntity<MensagemDeErro> paisInexistenteExceptionHandler(PaisInexistenteException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("País não cadastrado",e.getMessage(),400,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
	}
	
	@ExceptionHandler(LocalInexistenteException.class)
	public ResponseEntity<MensagemDeErro> localInexistenteExceptionHandler(LocalInexistenteException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Local não cadastrado",e.getMessage(),400,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
	}
	
	@ExceptionHandler(OponentesInvalidosException.class)
	public ResponseEntity<MensagemDeErro> oponentesInvalidosExceptionHandler(OponentesInvalidosException e, HttpServletRequest req){
		MensagemDeErro msg = new MensagemDeErro("Oponentes Inválidos",e.getMessage(),400,e.getClass().getName());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
	}
			
	
}
