package com.ciandt.app.exceptions;

public class PaisInexistenteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9030795564547233646L;
	
	public PaisInexistenteException() {
		super();
	}
	
	public PaisInexistenteException(String mensagem) {
		super(mensagem);
	}

}
