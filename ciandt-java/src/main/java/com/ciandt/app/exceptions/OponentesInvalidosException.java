package com.ciandt.app.exceptions;

public class OponentesInvalidosException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8285788186567047515L;
	
	public OponentesInvalidosException() {
		super();
	}
	
	public OponentesInvalidosException(String mensagem) {
		super(mensagem);
	}

}
