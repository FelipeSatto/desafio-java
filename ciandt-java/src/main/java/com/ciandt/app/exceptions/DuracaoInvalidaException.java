package com.ciandt.app.exceptions;

public class DuracaoInvalidaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7007806459380195867L;
	
	public DuracaoInvalidaException() {
		super();
	}
	
	public DuracaoInvalidaException(String mensagem) {
		super(mensagem);
	}

}
