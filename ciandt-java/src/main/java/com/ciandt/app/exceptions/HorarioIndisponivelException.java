package com.ciandt.app.exceptions;

public class HorarioIndisponivelException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3281421827088718953L;
	
	public HorarioIndisponivelException() {
		super();
	}
	
	public HorarioIndisponivelException(String mensagem) {
		super(mensagem);
	}

}
