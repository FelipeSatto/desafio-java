package com.ciandt.app.exceptions;

public class CampoNuloException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7286292205911247910L;
	
	public CampoNuloException() {
		super();
	}
	
	public CampoNuloException(String mensagem) {
		super(mensagem);
	}

}
