package com.ciandt.app.exceptions;

public class LocalInexistenteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6351578455766032059L;
	
	public LocalInexistenteException() {
		super();
	}
	
	public LocalInexistenteException(String mensagem) {
		super(mensagem);
	}

}
