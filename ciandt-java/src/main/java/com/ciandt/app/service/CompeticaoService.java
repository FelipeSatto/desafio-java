package com.ciandt.app.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.app.domain.Competicao;
import com.ciandt.app.domain.Etapa;
import com.ciandt.app.domain.Local;
import com.ciandt.app.domain.Modalidade;
import com.ciandt.app.domain.Pais;
import com.ciandt.app.exceptions.CompeticaoNaoEncontradaException;
import com.ciandt.app.exceptions.LocalInexistenteException;
import com.ciandt.app.exceptions.ModalidadeInvalidaException;
import com.ciandt.app.exceptions.OponentesInvalidosException;
import com.ciandt.app.exceptions.PaisInexistenteException;
import com.ciandt.app.repository.CompeticaoRepository;
import com.ciandt.app.repository.LocalRepository;
import com.ciandt.app.repository.PaisRepository;
import com.ciandt.app.util.CompeticaoValidacao;


@Service
public class CompeticaoService {
	
	@Autowired
	private CompeticaoRepository competicaoRepo;
	
	@Autowired
	private LocalRepository localRepo;
	
	@Autowired
	private PaisRepository paisRepo;
	
	/*
	 * Método para salvar uma nova competição
	 * 
	 * Parâmetros:
	 * 	- comp: objeto de competição para ser persistido na base
	 * 
	 * Retorno:
	 * 	- objeto de competição persistido na base
	 */
	public Competicao salvarCompeticao(Competicao comp) {
		Competicao aux = null;
		//verifica se todos os campos não nulos estão preenchidos
		if(CompeticaoValidacao.validarCamposNaoNulos(comp)) {
			//verifica se a duração informada é válida
			if(CompeticaoValidacao.validarDuracao(comp)) {
				//Verifica se o horário está disponível no local
				if(CompeticaoValidacao.validarHorarioDisponivel(comp,competicaoRepo)) {
					//Verifica se o local possui menos de 4 competições naquele dia
					if(CompeticaoValidacao.validarMaximoDeCompeticoesNoLocal(comp,competicaoRepo)) {
						//verifica se os países estão cadastrados
						Pais paisUm = paisRepo.findByNomeIgnoreCase(comp.getPaisUm());
						Pais paisDois = paisRepo.findByNomeIgnoreCase(comp.getPaisDois());
						if(paisUm == null) {
							throw new PaisInexistenteException("País "+comp.getPaisUm()+" não encontrado na base. Primeiramente cadestre-o no endereço POST /pais");
						}else if(paisDois == null) {
							throw new PaisInexistenteException("País "+comp.getPaisUm()+" não encontrado na base. Primeiramente cadestre-o no endereço POST /pais");
						}
						
						//Verifica se os dois países tem o mesmo nome e não é etapa final ou semifinal
						if((comp.getPaisUm().equals(comp.getPaisDois())) && (!comp.getEtapa().equals(Etapa.FINAL) || !comp.getEtapa().equals(Etapa.SEMIFINAL))){
							throw new OponentesInvalidosException("Não se podem ter dois oponentes do mesmo país sem ser nas semifinais ou finais");
						}
						//Verifica se o local está cadastrado
						Local loc = localRepo.findOne(comp.getLocal().getId());
						if(loc == null) {
							throw new LocalInexistenteException("Local informado não existe na base. Primeiramente cadastre-o no caminho POST /caminho");
						}					
						
						aux = competicaoRepo.save(comp);
						return aux;
					}
				}
			}
		}
		
		return null;
	}
	
	
	/*
	 * Método para pesquisar as competições
	 * 
	 * Parâmetros:
	 * 	- mod: string opiciona que representa a modalidade a ser buscada. 
	 * 
	 * Retornos:
	 *  - para mod informado retorna todas as competições para a quela modalidade
	 *  - para mod não informado retorna todas as competições
	 */
	public List<Competicao> buscarCompeticoes(String mod){
		List<Competicao> listaRetorno;
		if(mod==null || mod.equals("")) {
			listaRetorno = competicaoRepo.findAllOrderByDiaAndInicio();			
		}else {
			try {
				Modalidade modBusca = Modalidade.valueOf(mod.toUpperCase());
				listaRetorno = competicaoRepo.findByModalidadeOrderByDiaAscInicioAsc(modBusca);				
			}catch(Exception e) {
				throw new ModalidadeInvalidaException("A modalidade informada não existe");
			}
		}
		if(listaRetorno.isEmpty()) {
			throw new CompeticaoNaoEncontradaException("Não foram encontradas competições para esta modalidade");
		}
		return listaRetorno;
	}

	/*
	 * Método auxiliar para pré cadastrar os locais na base
	 */
	public List<Local> criarLocaisParaTeste() {
		List<Local> listaRetorno = new ArrayList<>();
		Local aux = new Local();
		
		aux.setNome("Ryogoku Kokugikan");
		aux.setEndereco("endereço de Ryogoku Kokugikan");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Nippon Budokan");
		aux.setEndereco("endereço de Nippon Budokan");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("National Yoyogi Stadium");
		aux.setEndereco("endereço de National Yoyogi Stadium");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Korakuen Hall");
		aux.setEndereco("endereço de Korakuen Hall");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Tokyo Metropolitan Gymnasium");
		aux.setEndereco("endereço de Tokyo Metropolitan Gymnasium");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Ajinomoto Field Nishigaoka");
		aux.setEndereco("endereço de Ajinomoto Field Nishigaoka");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Rinkai Ball Field");
		aux.setEndereco("endereço de Rinkai Ball Field");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Ota Stadium");
		aux.setEndereco("endereço de Ota Stadium");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Edogawa Baseball Field");
		aux.setEndereco("endereço de Edogawa Baseball Field");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Jingu Second Stadium");
		aux.setEndereco("endereço de Jingu Second Stadium");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		aux = new Local();		
		aux.setNome("Edogawa Athletic Stadium");
		aux.setEndereco("endereço de Edogawa Athletic Stadium");
		localRepo.save(aux);
		listaRetorno.add(aux);
		
		return listaRetorno;
	}
	
	public List<Pais> criarPaisesParaTeste(){
		List<Pais> paises = new ArrayList<>();
		Pais aux;
		
		aux = new Pais();
		aux.setNome("Estados Unidos da América");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Brasil");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("China");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Japão");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Alemanha");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("França");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Uruguai");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Paraguai");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Inglaterra");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Espanha");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Portugal");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Rússia");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Australia");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Venezuela");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("África do Sul");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Congo");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Gana");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Egito");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("México");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Israel");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Índia");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		aux = new Pais();
		aux.setNome("Nova Zelândia");
		aux = paisRepo.save(aux);
		paises.add(aux);
		
		return paises;
	}
	
	
	
}
