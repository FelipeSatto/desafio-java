package com.ciandt.app.util;

import java.sql.Time;
import java.time.temporal.ChronoUnit;
import java.util.List;

import com.ciandt.app.domain.Competicao;
import com.ciandt.app.exceptions.CampoNuloException;
import com.ciandt.app.exceptions.DuracaoInvalidaException;
import com.ciandt.app.exceptions.HorarioIndisponivelException;
import com.ciandt.app.exceptions.MaximoDeCompeticoesException;
import com.ciandt.app.repository.CompeticaoRepository;

public class CompeticaoValidacao {
	
	public static final long duracaoMinima = 30;

	/*
	 * Método para verificar se todos os campos obrigatórios foram preenchidos
	 * 
	 * Parâmetros
	 * 	- comp: objeto de competição para ser verificado
	 * Retorno
	 * 	- retorna true caso todos os campos estejam preenchidos corretamente
	 * Exceptions
	 * 	- retorna CampoNuloException caso algum campo obrigatório não esteja preenchido
	 */
	public static boolean validarCamposNaoNulos(Competicao comp) {
		System.out.println(comp.getInicio());
		if(comp.getInicio() == null)
			campoNuloException("inicio");
		if(comp.getFim() == null)
			campoNuloException("fim");
		if(comp.getModalidade() == null)
			campoNuloException("modalidade");
		if(comp.getEtapa() == null)
			campoNuloException("etapa");
		if(comp.getLocal() == null)
			campoNuloException("local");
		if(comp.getPaisUm() == null)
			campoNuloException("país um");
		if(comp.getPaisDois() == null)
			campoNuloException("país dois");
		return true;
	}
	
	/*
	 * Método para verificar a duração da competição
	 * 
	 * Parâmetros
	 * 	- comp: objeto de competição para ser verificado
	 * Retorno
	 * 	- retorna true caso a duração da competição for superior ou igual a 30 minutos
	 * Exceptions
	 * 	- dispara DuracaoInvalidaException caso a duração seja inferior a 30 minutos
	 */
	public static boolean validarDuracao(Competicao comp) {
		Time inicio = comp.getInicio();
		Time fim = comp.getFim();		
		
		if(calcularDuracao(inicio, fim)<duracaoMinima)
			throw new DuracaoInvalidaException("A duração é inferior a 30 minutos. Forneça uma duração maior que 30 minutos");
		
		return true;
	}

	public static long calcularDuracao(Time inicio, Time fim) {
		return inicio.toLocalTime().until(fim.toLocalTime(), ChronoUnit.MINUTES);
	}
	
	/*
	 * Método para verificar se o horário está disponível para aquele local
	 */
	public static boolean validarHorarioDisponivel(Competicao comp,CompeticaoRepository repoRef) {
		String inicio = comp.getInicio().toString();
		String fim = comp.getFim().toString();
		List<Competicao> retorno = repoRef.findPorHorarioDiaLocalModalidade(comp.getDia(), comp.getLocal(),comp.getModalidade().toString(), inicio, fim);
		
		if(retorno.isEmpty()) {
			return true;
		}else {
			throw new HorarioIndisponivelException("O horário informado já possui uma competição registrada na modalidade "+comp.getModalidade().toString());
		}
	}
	
	/*
	 * Método para verificar se o local já não tem 4 competições no dia
	 */
	public static boolean validarMaximoDeCompeticoesNoLocal(Competicao comp,CompeticaoRepository repoRef) {
		List<Competicao> comps = repoRef.findByDiaAndLocal(comp.getDia(), comp.getLocal());
		if(comps != null && comps.size()>=4) {
			throw new MaximoDeCompeticoesException("O local informado já possui 4 competições registradas");
		}
		return true;
	}
	
	/*
	 * Método para disparar a CampoNuloException
	 */
	public static void campoNuloException(String nomeDoCampo) {
		throw new CampoNuloException("O campo "+nomeDoCampo+" está vazio. Por favor o preencha para poder cadastrar.");
	}

	
}
