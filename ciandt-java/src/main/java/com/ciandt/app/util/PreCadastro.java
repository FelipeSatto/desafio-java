package com.ciandt.app.util;

import java.util.List;

import com.ciandt.app.domain.Local;
import com.ciandt.app.domain.Pais;

public class PreCadastro {
	
	private List<Local> locais;
	private List<Pais> paises;
	
	public PreCadastro() {
		
	}

	public List<Local> getLocais() {
		return locais;
	}

	public void setLocais(List<Local> locais) {
		this.locais = locais;
	}

	public List<Pais> getPaises() {
		return paises;
	}

	public void setPaises(List<Pais> paises) {
		this.paises = paises;
	}

}
