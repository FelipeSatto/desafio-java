package com.ciandt.app.util;

public class MensagemDeErro {
	
	private String titulo;
	private String mensagem;
	private int status;
	private String exception;
	
	public MensagemDeErro() {
		
	}

	public MensagemDeErro(String titulo, String mensagem, int status, String exception) {
		super();
		this.titulo = titulo;
		this.mensagem = mensagem;
		this.status = status;
		this.exception = exception;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}
	
}
